import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class User {
	@PrimaryGeneratedColumn('increment')
	id: number;

	@Column({length : 20})
	username: string;

	@Column({length:100})
	password: string;

	@Column()
	aktif: number;
}
