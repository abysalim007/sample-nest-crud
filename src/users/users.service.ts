import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';

import * as bcrypt from 'bcrypt';

/*crud : see typeorm cheatsheet*/

@Injectable()
export class UsersService {

  constructor(
    @InjectRepository(User)
    private usersRepository:Repository<User>
  ){}

  async create(createUserDto: CreateUserDto) {
    // return 'This action adds a new user';
    /*hash password*/
    const round = 10;
    const password = createUserDto.password;
    const hashPwd = await bcrypt.hash(password, round);
    const saveData = createUserDto;
    saveData.password = hashPwd;
    const sv = this.usersRepository.save(createUserDto);
    if(sv){
      return this.getResponse(200,'created.',sv);  
    }else{
      return this.getResponse(500,'fail. cannot create data.',[]);
    }
    
  }


  async findAll() {
    // return `This action returns all users`;
    const data = await this.usersRepository.find();
    if(!data){
      return this.getResponse(404,'data doesn`t exist.',[]);
    }else{
      return this.getResponse(200,'data exist.',data);
    }
    
  }

  async findOne(theId: number) {
    // return `This action returns a #${id} user`;
    let data = await this.usersRepository.findOne(
        { where:
            { id: theId }
        }
    ); 
    
    if(!data){
      return this.getResponse(404,'data doesn`t exist.',[]);
    }else{
      return this.getResponse(200,'data exist.',data);
    }
    
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    const [userData, userCount] = await this.usersRepository.findAndCount({
      where:
            { id: id }
    });

    if(userCount>0){
      /*ready Update*/
      const round = 10;
      const password = updateUserDto.password;
      const hashPwd = await bcrypt.hash(password, round);
      
      userData[0].username = updateUserDto.username;
      userData[0].password = hashPwd;
      userData[0].aktif = updateUserDto.aktif;
      const saveData =  this.usersRepository.save(userData);
      return this.getResponse(200,'saved.',saveData);
    }else{
      return this.getResponse(500,'fail. data doesn`t exist.',[]);
    }


  }

  async remove(id: number) {
    // return `This action removes a #${id} user`;
    let [userData, userCount] = await this.usersRepository.findAndCount({
      where:
            { id: id }
    });

    if(userCount>0){
      /*ready delete*/
      const rmv = this.usersRepository.remove(userData);
      return this.getResponse(200,'removed.',rmv);
    }else{
      return this.getResponse(500,'fail. data doesn`t exist.',[]);
    }

  }

  async login(){

  }

  async getToken(){

  }

  getResponse(code=200,msg='',data){
    return {
      'code' : code,
      'msg' : msg,
      'data' : data
    }

  }
}
