import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import 'dotenv/config';

const db_host = process.env.DB_HOST;
const db_port = process.env.DB_PORT;
const db_user = process.env.DB_USER;
const db_pass = process.env.DB_PASS;
const db_name = process.env.DB_NAME;

@Module({
  imports: [
  	/* module db from typeORM */
  	TypeOrmModule.forRoot({
  		type: 'mysql',
  		host: db_host,
  		port: parseInt(db_port),
  		username: db_user,
  		password: db_pass,
  		database: db_name,
      	// entities: ['src/**/entities/*.entity.ts','dist/**/entities/*.entity.js',],
      	// entities: [__dirname + '/../**/*.entity.{js,ts}'],
      	entities: ["dist/**/entities/*.js"],
  		synchronize: true,
  	}),
  	UsersModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
