import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import 'dotenv/config';

const app_port = process.env.PORT;
async function bootstrap() {
  const app = await NestFactory.create(AppModule,{
  logger: console,
});
  await app.listen(parseInt(app_port));
}
bootstrap();
